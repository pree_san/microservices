import pika, json, os, django

# os diperlukan karena file products berada di luar direktori ini
# sehingga file tersebut harus di panggil dengan setting os enivornment django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "admin.settings")
django.setup()

from products.models import Products

params = pika.URLParameters('amqps://pxmvechc:CtXdiYGZwaickidQMFK1c12cZXT_XBfm@armadillo.rmq.cloudamqp.com/pxmvechc')

connection = pika.BlockingConnection(params)

channel = connection.channel()

channel.queue_declare(queue='admin')

def callback(ch, method, properties, body):
    print("Received in admin")
    id = json.loads(body)
    print(id)
    product = Products.objects.get(id=id)
    product.likes = product.likes + 1
    product.save()
    print("Product likes increased!")

channel.basic_consume(queue='admin', on_message_callback=callback, auto_ack=True)

print("Started Consuming")

channel.start_consuming()

channel.close()